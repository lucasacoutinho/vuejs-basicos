# VueJS Básicos

Aprendizado do framework frontend VueJS, com base nos cursos 
 - **[Desenvolvimento Web Avançado 2021 com PHP, Laravel e Vue.JS](https://www.udemy.com/course/curso-completo-do-desenvolvedor-laravel/learn/lecture/26218634#overview)**
 - **[Build Web Apps with Vue JS 3 & Firebase](https://www.udemy.com/course/build-web-apps-with-vuejs-firebase/)**
 - **[VueJS documentação](https://vuejs.org/)**